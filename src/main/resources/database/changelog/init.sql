--liquibase formatted sql

--changeset towiza:init/1
CREATE SEQUENCE hibernate_sequence START 100 INCREMENT 1;

CREATE TABLE app_user
(
    id          INT NOT NULL,
    email       VARCHAR(255),
    username    VARCHAR(255),
    first_name  VARCHAR(255),
    last_name   VARCHAR(255),
    PASSWORD    VARCHAR(255),
    reset_token VARCHAR(255),
    LANGUAGE    VARCHAR(255),
    birth_place VARCHAR(255),
    birth_date  TIMESTAMP,
    ROLE        VARCHAR(255),
    user_active BOOLEAN,
    created_at  TIMESTAMP,
    PRIMARY KEY (id)
);

ALTER TABLE app_user
    ADD CONSTRAINT uk_email_in_table_app_user UNIQUE (email);

ALTER TABLE app_user
    ADD CONSTRAINT uk_username_in_table_app_user UNIQUE (username);

--changeset towiza:init/2

CREATE TABLE client
(
    id          INT NOT NULL,
    name VARCHAR(255),
    name_of_company VARCHAR(255),
    phone_number VARCHAR(255),
    address VARCHAR(255),
    note VARCHAR(255),
    date_inscription DATE,
    PRIMARY KEY (id)
);


CREATE TABLE supplier
(
    id          INT NOT NULL,
    name VARCHAR(255),
    name_of_company VARCHAR(255),
    phone_number VARCHAR(255),
    address VARCHAR(255),
    note VARCHAR(255),
    date_inscription DATE,
    PRIMARY KEY (id)
);

CREATE TABLE category
(
    id          INT NOT NULL,
    label VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE product
(
    id          INT NOT NULL,
    label VARCHAR(255),
    serie VARCHAR(255),
    note VARCHAR(255),
    category_id BIGINT,
    constraint product_category_fk
        foreign key (category_id) references category,
    PRIMARY KEY (id)
);

CREATE TABLE ordere
(
    id          INT NOT NULL,
    date_creation DATE,
    note TEXT,
    total_amount real,
    partial_amount  real,
    supplier_id  BIGINT,
    constraint order_client_fk
        foreign key (supplier_id) references supplier,
    PRIMARY KEY (id)
);



CREATE TABLE order_item
(
    id          INT NOT NULL,
    date_creation DATE,
    quantity INT,
    quantity_sold INT,
    returned_quantity INT,
    status VARCHAR(255),
    purchase_price real,
    sale_price real,
    product_id  BIGINT,
    order_id  BIGINT,
    constraint order_product_fk
        foreign key (product_id) references product,
    constraint order_order_item_fk
        foreign key (order_id) references ordere,
    PRIMARY KEY (id)
);

CREATE TABLE invoice
(
    id          INT NOT NULL,
    date_creation DATE,
    note TEXT,
    total_amount real,
    partial_amount  real,
    client_id  BIGINT,
    constraint invoice_client_fk
        foreign key (client_id) references client,
    PRIMARY KEY (id)
);


CREATE TABLE invoice_item
(
    id          INT NOT NULL,
    date_creation DATE,
    quantity INT,
    status VARCHAR(255),
    price REAL,
    sale_price REAL,
    product_id  BIGINT,
    invoice_id  BIGINT,
    order_item_id  BIGINT,
    constraint invoice_product_fk
        foreign key (product_id) references product,
    constraint order_item_order_fk
        foreign key (invoice_id) references invoice,
    constraint invoice_order_item_fk
        foreign key (order_item_id) references order_item,
    PRIMARY KEY (id)
);

CREATE TABLE avoir
(
    id          INT NOT NULL,
    date_creation DATE,
    note TEXT,
    total_amount real,
    partial_amount  real,
    supplier_id  BIGINT,
    constraint avoir_supplier_fk
        foreign key (supplier_id) references supplier,
    PRIMARY KEY (id)
);

CREATE TABLE avoir_item
(
    id          INT NOT NULL,
    date_creation DATE,
    quantity INT,
    status VARCHAR(255),
    price REAL,
    avoir_id  BIGINT,
    order_item_id  BIGINT,
    constraint avoir_item_avoir_fk
        foreign key (avoir_id) references avoir,
    constraint avoir_order_item_fk
        foreign key (order_item_id) references order_item,
    PRIMARY KEY (id)
);

--changeset towiza:init/3

alter table ordere add column status VARCHAR(255);

alter table invoice add column status VARCHAR(255);

alter table avoir add column status VARCHAR(255);

--changeset towiza:init/4

CREATE TABLE transaction
(
    id          INT NOT NULL,
    date_creation       DATE,
    note                TEXT,
    to_me               BOOLEAN,
    status              VARCHAR(255),
    busines_type        VARCHAR(255),
    busines_id          BIGINT,
    amount              REAL,
    devise              REAL,
    PRIMARY KEY (id)
);

--changeset towiza:init/5

CREATE TABLE caisse
(
    id          INT NOT NULL,
    date_creation       DATE,
    note                TEXT,
    to_me               BOOLEAN,
    status              VARCHAR(255),
    amount              REAL,
    devise              REAL,
    PRIMARY KEY (id)
);
