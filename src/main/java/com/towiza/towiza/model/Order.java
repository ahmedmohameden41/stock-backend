package com.towiza.towiza.model;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ordere")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "note")
    private String note;

    @Column(name = "status")
    private String status;

    @JoinColumn(name = "order_id")
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<OrderItem> orderItems;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @Column(name = "total_amount")
    private double totalAmount;

    @Column(name = "partial_amount")
    private double partialAmount;

}
