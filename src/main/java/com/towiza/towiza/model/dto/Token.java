package com.towiza.towiza.model.dto;


import com.towiza.towiza.security.TokenUser;

public class Token {
    public boolean authenticated = false;
    public String authToken;
    public TokenUser user;
    public String refreshToken;
}
