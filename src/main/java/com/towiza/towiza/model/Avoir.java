package com.towiza.towiza.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "avoir")
public class Avoir {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "note")
    private String note;

    @Column(name = "status")
    private String status;

    @JsonIgnore
    @JoinColumn(name = "avoir_id")
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<AvoirItem> avoirItems;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @Column(name = "total_amount")
    private double totalAmount;

    @Column(name = "partial_amount")
    private double partialAmount;

}
