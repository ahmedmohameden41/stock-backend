package com.towiza.towiza.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "caisse")
public class Case {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "note")
    private String note;

    @Column(name = "to_me")
    private Boolean toMe;

    @Column(name = "status")
    private String status;


    @Column(name = "amount")
    private double amount;

    @Column(name = "devise")
    private double devise;
}
