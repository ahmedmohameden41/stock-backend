package com.towiza.towiza.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "invoice")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "note")
    private String note;

    @Column(name = "status")
    private Boolean status;

    @JoinColumn(name = "invoice_id")
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<InvoiceItem> invoiceItems;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "total_amount")
    private double totalAmount;

    @Column(name = "partial_amount")
    private double partialAmount;

}
