package com.towiza.towiza.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "note")
    private String note;

    @Column(name = "to_me")
    private Boolean toMe;

    @Column(name = "status")
    private String status;

    @Column(name = "busines_type")
    private String businesType;

    @Column(name = "busines_id")
    private Long businesId;

    @Column(name = "amount")
    private double amount;

    @Column(name = "devise")
    private double devise;
}
