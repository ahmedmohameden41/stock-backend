package com.towiza.towiza.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.towiza.towiza.model.User;
import com.towiza.towiza.model.dto.Token;
import com.towiza.towiza.model.dto.UserPass;
import com.towiza.towiza.repository.UserRepository;
import com.towiza.towiza.security.JwtProperties;
import com.towiza.towiza.security.TokenUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class AuthService {

    private final Logger log = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User register(User user) {

        if (userExists(user.getUsername())) {
            throw new IllegalArgumentException(String.format("Ce username %s existe déja", user.getUsername()));
        }
        if (emailExists(user.getEmail())) {
            throw new IllegalArgumentException(String.format("Email %s already exists", user.getEmail()));
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setUserActive(false);
        if (user.getLanguage() == null) user.setLanguage("fr");
        return repository.save(user);
    }

    private boolean emailExists(String email) {
        Optional<User> emailExists = repository.findByEmail(email);
        return emailExists.isPresent();
    }

    public boolean userExists(String username) {
        Optional<User> userExists = repository.findByUsername(username);
        return userExists.isPresent();
    }

    public ResponseEntity<Token> add(UserPass userPass) {
        Token token = new Token();
        HttpStatus status = HttpStatus.FORBIDDEN;

        Optional<User> userOptional = repository.findByUsername(userPass.username);
        if (userOptional.isPresent() && isUserValid(userOptional.get(), userPass.password)) {
            User user = userOptional.get();
            try {
                TokenUser tokenUser = user.toTokenUser();

                status = HttpStatus.OK;
                token.user = user.toTokenUser();
                token.authenticated = true;
                token.authToken =
                        JwtProperties.generateToken(tokenUser, false, true);
                token.refreshToken =
                        JwtProperties.generateToken(tokenUser, true, true);
            } catch (JsonProcessingException e) {
                log.error("Unable to generate auth token {}", Arrays.toString(e.getStackTrace()));
            }
        }
        return ResponseEntity.status(status).body(token);
    }

    private boolean isUserValid(User user, String password) {
        return user != null &&
                user.getUserActive() != null && user.getUserActive() &&
                passwordEncoder.matches(password, user.getPassword());
    }

}
