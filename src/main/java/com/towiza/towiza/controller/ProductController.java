package com.towiza.towiza.controller;

import com.towiza.towiza.model.Product;
import com.towiza.towiza.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @PostMapping
    public ResponseEntity<List<Product>> saveAllProducts(@RequestBody List<Product> products) {
        return ResponseEntity.ok().body(productRepository.saveAll(products));
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllClients() {
        return ResponseEntity.ok().body(productRepository.findAll());
    }

    @GetMapping("/delete-product/{id}")
    public ResponseEntity<Boolean> deleteProductById(@PathVariable(value = "id") Long id) {
        productRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }

}
