package com.towiza.towiza.controller;

import com.towiza.towiza.model.User;
import com.towiza.towiza.model.dto.Token;
import com.towiza.towiza.model.dto.UserPass;
import com.towiza.towiza.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService service;

    @PostMapping("/register")
    public User register(@RequestBody User user) {
        return service.register(user);
    }

    @PostMapping("/token")
    public ResponseEntity<Token> add(@RequestBody UserPass userPass) {
        return service.add(userPass);
    }
}
