package com.towiza.towiza.controller;

import com.towiza.towiza.model.Transaction;
import com.towiza.towiza.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/transactions")
public class TransactionController {

    @Autowired
    TransactionRepository transactionRepository;


    @PostMapping
    public ResponseEntity<Transaction> saveAllTransactions(@RequestBody Transaction transaction) {
        transaction.setDateCreation(LocalDate.now());
        return ResponseEntity.ok().body(transactionRepository.save(transaction));
    }

    @GetMapping
    public ResponseEntity<List<Transaction>> getAllTransaction() {
        return ResponseEntity.ok().body(transactionRepository.findAll());
    }


    @GetMapping("/by-busines-type-and-id/{businesType}/{businesId}")
    public ResponseEntity<List<Transaction>> getAllTransactionByBusinesTypeAndBusinesId(@PathVariable(value = "businesType") String businesType,
                                                               @PathVariable(value = "businesId") Long businesId) {
        return ResponseEntity.ok().body(transactionRepository.findAllByBusinesTypeAndBusinesId(businesType, businesId));
    }


    @GetMapping("/delete-transaction/{id}")
    public ResponseEntity<Boolean> deleteTransactionById(@PathVariable(value = "id") Long id) {
        transactionRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }
}
