package com.towiza.towiza.controller;

import com.towiza.towiza.model.Supplier;
import com.towiza.towiza.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/supplier")
public class SupplierController {

    @Autowired
    SupplierRepository supplierRepository;

    @PostMapping
    public ResponseEntity<List<Supplier>> saveAllSuppliers(@RequestBody List<Supplier> suppliers) {
        suppliers.forEach(supplier -> {
            if (supplier.getId() == null) {
                supplier.setDateInscription(LocalDate.now());
            }
        });
        return ResponseEntity.ok().body(supplierRepository.saveAll(suppliers));
    }

    @GetMapping
    public ResponseEntity<List<Supplier>> getAllSuppliers() {
        return ResponseEntity.ok().body(supplierRepository.findAll());
    }
}
