package com.towiza.towiza.controller;

import com.towiza.towiza.model.*;
import com.towiza.towiza.repository.*;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {

    @Autowired
    OrderRepository orderRepository;


    @Autowired
    OrderItemRepository orderItemRepository;


    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ProductRepository productRepository;

    @PostMapping
    public ResponseEntity<Order> saveAllCategories(@RequestBody Order order) {
        order.setDateCreation(LocalDate.now());
        Supplier supplier = supplierRepository.findById(order.getSupplier().getId())
                .orElseThrow( () -> new IllegalStateException("C pas possible"));
        order.getOrderItems().forEach(orderItem -> {
            Product product = productRepository.findById(orderItem.getProduct().getId()).get();
            orderItem.setProduct(product);

        });
        order.setSupplier(supplier);
        orderRepository.save(order);
        return ResponseEntity.ok().body(order);
    }

    @GetMapping
    public ResponseEntity<List<Order>> getAllCategories() {
        return ResponseEntity.ok().body(orderRepository.findAll());
    }


    @GetMapping("/by-supplier-id/{supplierId}")
    public ResponseEntity<List<Order>> findAllByClientId(@PathVariable(value = "supplierId") Long supplierId) {
        return ResponseEntity.ok().body(orderRepository.findAllBySupplier_Id(supplierId).stream().filter(elem -> Objects.equals(elem.getStatus(), "STOCK")).collect(Collectors.toList()));
    }



    @GetMapping("/delete-order/{id}")
    public ResponseEntity<Boolean> deleteOrderById(@PathVariable(value = "id") Long id) {
        orderRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }
}
