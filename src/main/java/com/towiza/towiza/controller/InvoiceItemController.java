package com.towiza.towiza.controller;

import com.towiza.towiza.model.OrderItem;
import com.towiza.towiza.repository.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/invoice-item")
public class InvoiceItemController {

    @Autowired
    OrderItemRepository orderItemRepository;

    @PostMapping
    public ResponseEntity<List<OrderItem>> saveAllCategories(@RequestBody List<OrderItem> orders) {
        return ResponseEntity.ok().body(orderItemRepository.saveAll(orders));
    }

    @GetMapping
    public ResponseEntity<List<OrderItem>> getAllCategories() {
        return ResponseEntity.ok().body(orderItemRepository.findAll());
    }
/*
    @GetMapping("/by-order/{id}")
    public ResponseEntity<List<OrderItem>> findAllByOrderId(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok().body(orderItemRepository.findAllByOrder_Id(id));
    }*/
}
