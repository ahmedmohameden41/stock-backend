package com.towiza.towiza.controller;

import com.towiza.towiza.model.Client;
import com.towiza.towiza.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/clients")
public class ClientController {

    @Autowired
    ClientRepository clientRepository;

    @PostMapping
    public ResponseEntity<List<Client>> saveAllClients(@RequestBody List<Client> clients) {
        clients.forEach(client -> {
            if (client.getId() == null) {
                client.setDateInscription(LocalDate.now());
            }
        });
        return ResponseEntity.ok().body(clientRepository.saveAll(clients));
    }

    @GetMapping
    public ResponseEntity<List<Client>> getAllClients() {
        return ResponseEntity.ok().body(clientRepository.findAll());
    }
}
