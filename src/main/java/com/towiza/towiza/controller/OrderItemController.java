package com.towiza.towiza.controller;

import com.towiza.towiza.model.OrderItem;
import com.towiza.towiza.repository.OrderItemRepository;
import com.towiza.towiza.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/v1/order-item")
public class OrderItemController {

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    OrderRepository orderRepository;

    @PostMapping
    public ResponseEntity<List<OrderItem>> saveAllCategories(@RequestBody List<OrderItem> orders) {
        return ResponseEntity.ok().body(orderItemRepository.saveAll(orders));
    }

    @GetMapping
    public ResponseEntity<List<OrderItem>> getAllCategories() {
        return ResponseEntity.ok().body(orderItemRepository.findAll());
    }

    @GetMapping("/stocks")
    public ResponseEntity<List<OrderItem>> findAllByOrderId() {
        List<OrderItem> orderItemList = new ArrayList<>();
        orderRepository.findAll().forEach(order -> {
                    if (order.getStatus().equals("STOCK")) {
                        orderItemList.addAll(order.getOrderItems());
                    }
                }
        );
        return ResponseEntity.ok().body(orderItemList.stream().filter(elem -> elem.getQuantity() != (elem.getQuantitySold() + elem.getReturnedQuantity())).collect(Collectors.toList()));
    }
}
