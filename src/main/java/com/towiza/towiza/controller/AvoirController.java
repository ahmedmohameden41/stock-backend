package com.towiza.towiza.controller;

import com.towiza.towiza.model.Avoir;
import com.towiza.towiza.model.OrderItem;
import com.towiza.towiza.model.Supplier;
import com.towiza.towiza.repository.AvoirRepository;
import com.towiza.towiza.repository.OrderItemRepository;
import com.towiza.towiza.repository.ProductRepository;
import com.towiza.towiza.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/avoirs")
public class AvoirController {

    @Autowired
    AvoirRepository avoirRepository;


    @Autowired
    OrderItemRepository orderItemRepository;


    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ProductRepository productRepository;


    @PostMapping
    public ResponseEntity<Avoir> saveAllCategories(@RequestBody Avoir avoir) {
        avoir.setDateCreation(LocalDate.now());
        Supplier supplier = supplierRepository.findById(avoir.getSupplier().getId())
                .orElseThrow(() -> new IllegalStateException("C pas possible"));
        avoir.getAvoirItems().forEach(avoirItem -> {
            OrderItem orderItem = orderItemRepository.findById(avoirItem.getOrderItem().getId()).get();
            orderItem.setReturnedQuantity(orderItem.getReturnedQuantity() + avoirItem.getQuantity());
            avoirItem.setOrderItem(orderItem);
        });
        avoir.setSupplier(supplier);
        avoirRepository.save(avoir);
        return ResponseEntity.ok().body(avoir);
    }

    @GetMapping
    public ResponseEntity<List<Avoir>> getAllCategories() {
        return ResponseEntity.ok().body(avoirRepository.findAll());
    }
}
