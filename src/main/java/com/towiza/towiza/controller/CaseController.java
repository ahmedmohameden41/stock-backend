package com.towiza.towiza.controller;

import com.towiza.towiza.model.Case;
import com.towiza.towiza.repository.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/cases")
public class CaseController {

    @Autowired
    CaseRepository caseRepository;


    @PostMapping
    public ResponseEntity<Case> saveCase(@RequestBody Case c) {
        c.setDateCreation(LocalDate.now());
        return ResponseEntity.ok().body(caseRepository.save(c));
    }

    @GetMapping
    public ResponseEntity<List<Case>> getAllCases() {
        return ResponseEntity.ok().body(caseRepository.findAll());
    }

    @GetMapping("/delete-case/{id}")
    public ResponseEntity<Boolean> deleteCaseById(@PathVariable(value = "id") Long id) {
        caseRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }
}
