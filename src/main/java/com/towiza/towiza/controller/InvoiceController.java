package com.towiza.towiza.controller;

import com.towiza.towiza.model.Client;
import com.towiza.towiza.model.Invoice;
import com.towiza.towiza.model.OrderItem;
import com.towiza.towiza.model.Product;
import com.towiza.towiza.repository.ClientRepository;
import com.towiza.towiza.repository.InvoiceRepository;
import com.towiza.towiza.repository.OrderItemRepository;
import com.towiza.towiza.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {

    @Autowired
    InvoiceRepository invoiceRepository;


    @Autowired
    OrderItemRepository orderItemRepository;


    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ProductRepository productRepository;


    @PostMapping
    public ResponseEntity<Invoice> saveAllCategories(@RequestBody Invoice invoice) {
        invoice.setDateCreation(LocalDate.now());
        Client client = clientRepository.findById(invoice.getClient().getId())
                .orElseThrow(() -> new IllegalStateException("C pas possible"));
        invoice.getInvoiceItems().forEach(invoiceItem -> {
            Product product = productRepository.findById(invoiceItem.getProduct().getId()).get();
            OrderItem orderItem = orderItemRepository.findById(invoiceItem.getOrderItem().getId()).get();
            orderItem.setQuantitySold(orderItem.getQuantitySold() + invoiceItem.getQuantity());
            invoiceItem.setProduct(product);
            invoiceItem.setOrderItem(orderItem);
        });
        invoice.setClient(client);
        invoiceRepository.save(invoice);
        return ResponseEntity.ok().body(invoice);
    }

    @GetMapping
    public ResponseEntity<List<Invoice>> getAllCategories() {
        return ResponseEntity.ok().body(invoiceRepository.findAll());
    }


    @GetMapping("/by-client-id/{clientId}")
    public ResponseEntity<List<Invoice>> findAllByClientId(@PathVariable(value = "clientId") Long clientId) {
        return ResponseEntity.ok().body(invoiceRepository.findAllByClient_Id(clientId));
    }

    @GetMapping("/delete-invoice/{id}")
    public ResponseEntity<Boolean> deleteInvoiceById(@PathVariable(value = "id") Long id) {
        invoiceRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }
}
