package com.towiza.towiza.controller;

import com.towiza.towiza.model.Category;
import com.towiza.towiza.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @PostMapping
    public ResponseEntity<List<Category>> saveAllCategories(@RequestBody List<Category> categories) {
        return ResponseEntity.ok().body(categoryRepository.saveAll(categories));
    }

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategories() {
        return ResponseEntity.ok().body(categoryRepository.findAll());
    }


    @GetMapping("/delete-category/{id}")
    public ResponseEntity<Boolean> deleteCategoryById(@PathVariable(value = "id") Long id) {
        categoryRepository.deleteById(id);
        return ResponseEntity.ok().body(true);
    }
}
