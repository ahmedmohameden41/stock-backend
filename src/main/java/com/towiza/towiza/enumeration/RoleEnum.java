package com.towiza.towiza.enumeration;

public enum RoleEnum {
    ADMIN,
    AGENT,
    SAMPLER,
    USER
}
