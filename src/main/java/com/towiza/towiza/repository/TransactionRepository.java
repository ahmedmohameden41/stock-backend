package com.towiza.towiza.repository;

import com.towiza.towiza.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByBusinesTypeAndBusinesId(String b, Long id);
}
