package com.towiza.towiza.repository;

import com.towiza.towiza.model.Avoir;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvoirRepository extends JpaRepository<Avoir, Long> {
}
