package com.towiza.towiza.repository;

import com.towiza.towiza.model.Case;
import com.towiza.towiza.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseRepository extends JpaRepository<Case, Long> {
}
