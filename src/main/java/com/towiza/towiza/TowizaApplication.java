package com.towiza.towiza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TowizaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TowizaApplication.class, args);
	}

}
